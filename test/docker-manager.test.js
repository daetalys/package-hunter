/* eslint-env mocha */

// const sinon = require('sinon')

const DockerManager = require('../src/docker-manager')

// ToDo: stub out calls to the docker api using sinon
describe.skip('DockerManager Test', function () {
  this.timeout(1200000)
  before(async function () {
    await new DockerManager().cleanup()
  })

  beforeEach(function () {
    this.manager = new DockerManager(process.cwd())
  })

  afterEach(async function () {
    await this.manager.cleanup()
  })

  it('_start', async function () {
    await this.manager._create('fass-0.0.1.tgz')
    await this.manager._start()
  })

  describe('_stop', function () {
    beforeEach(async function () {
      await this.manager._create()
      await this.manager._start()
    })

    it('docker stop', async function () {
      await this.manager._stop()
    })

    it('doesn\'t error if already stopped', async function () {
      await this.manager._stop()
      await this.manager._stop()
    })
  })

  it('docker cp', async function () {
    await this.manager._create('fass-0.0.1.tgz')
    await this.manager._cpIntoContainer(process.cwd() + '/fass-0.0.1.tgz', '/tmp')
  })

  it('docker run', async function () {
    await this.manager._create('csc-0.37.1.tgz')
  })

  it('docker exec', async function () {
    await this.manager._create()
    await this.manager._start()
    await this.manager._exec('csc-0.37.1.tgz')
  })

  it('cleans up container', async function () {
    await this.manager._create()
    await this.manager.cleanup()
  })

  it('does not error if nothing to cleanup', async function () {
    await this.manager.cleanup()
  })
})
