#!/bin/bash
set -e
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
PROTO_PATH="$SCRIPT_DIR/../protos"
FALCO_VERSION_DEFAULT=$(falco --version | grep "Falco version" | cut -d ":" -f2 | tr -d '[:space:]')
FALCO_VERSION=${1:-$FALCO_VERSION_DEFAULT}

function download_proto(){
  local falco_version=$1
  local protofile=$2

  echo "Downloading  $protofile"
  curl -L --fail "https://raw.githubusercontent.com/falcosecurity/falco/$falco_version/userspace/falco/$protofile" --output "$PROTO_PATH/$protofile"
}

# Latest Version can be found here:
# https://github.com/falcosecurity/falco/blob/master/userspace/falco/outputs.proto
download_proto "$FALCO_VERSION" outputs.proto
# Latest Version can be found here:
# https://github.com/falcosecurity/falco/blob/master/userspace/falco/schema.proto
download_proto "$FALCO_VERSION" schema.proto
