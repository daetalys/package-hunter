'use strict'

const util = require('util')
const fs = require('fs')
const path = require('path')
const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const debug = require('debug')('pkgs:bundler-middleware')

module.exports = function () {
  return async function (req, res, next) {
    // Remove any ruby version and engine specification in Gemfile (if present).
    let data
    try {
      data = await readFile(path.join(req.extractDestinationPath, 'Gemfile'))
    } catch (err) {
      res.status(500).json({
        status: 'error',
        reason: 'Unable to read Gemfile in project'
      })
      return
    }

    const lines = String(data).split('\n')
    const origLineNum = lines.length
    for (let i = 0; i < lines.length; i++) {
      if (lines[i].startsWith('ruby ')) {
        debug(`Removing Ruby version/engine specification in Gemfile: ${lines[i]}`)
        lines.splice(i, 1)
      }
    }

    if (lines.length < origLineNum) {
      const updatedLines = lines.join('\n')
      try {
        await writeFile(path.join(req.extractDestinationPath, 'Gemfile'), updatedLines)
      } catch (err) {
        res.status(500).json({
          status: 'error',
          reason: 'Unable to write modified Gemfile'
        })
        return
      }
    } else {
      debug('No Ruby version/engine specification detected in Gemfile')
    }
    next()
  }
}
