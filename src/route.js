'use strict'

const bodyParser = require('body-parser')
const basicAuth = require('express-basic-auth')
const conf = require('rc')('htr')
const debug = require('debug')('pkgs:route')

const authorizer = require('./authorizer.js')
const authMiddleware = basicAuth({ authorizer: authorizer(conf.users), authorizeAsync: true })
const createHandleBodyMiddleware = require('./handle-body-middleware.js')
const createDockerMiddleware = require('./docker-middleware.js')
const createFileExistsMiddleware = require('./file-exists-middleware.js')
const createBundlerMiddleware = require('./bundler-middleware.js')
const User = require('./user.js')

module.exports = function (app, opts) {
  app.use(bodyParser.raw({
    type: 'application/octet-stream',
    limit: '200mb' // size limit of body
  }))

  app.get('/', (req, res) => {
    debug('GET /')
    const id = req.query && req.query.id
    if (!id) {
      res.statusCode = 400
      res.json({ status: 'error', reason: 'required parameter \'id\' is missing' })
      return
    }

    if (!(id in opts.jobStore)) {
      debug(`no job with ${id} found`)
      res.json({ status: 'error', reason: `unknown id ${id}` })
      return
    }

    res.json(opts.jobStore[id])
  })

  if (process.env.NODE_ENV === 'development' && User.getAllUsers(conf.users).length === 0) {
    debug('Running in development mode. Authentication is disabled.')
  } else {
    app.post('/monitor/*', authMiddleware)
  }

  // Routes that analyze individual dependencies
  app.post('/monitor/dependency/yarn',
    createHandleBodyMiddleware(opts.jobStore, opts.workdir, { unpackOpts: { 'strip-components': 0 } }),
    createDockerMiddleware(opts.jobStore, opts.workdir, {
      dockerCpDestinationPath: '/dependency',
      dockerCreateOpts: {
        Cmd: ['yarn', 'add', '/dependency'],
        WorkingDir: '/my-package',
        HostConfig: {}
      }
    })
  )

  // Routes that analyze entire projects including all dependencies
  const handleBodyMiddleware = createHandleBodyMiddleware(opts.jobStore, opts.workdir, { unpackOpts: { 'strip-components': 1 } })
  app.post('/monitor/project/npm', handleBodyMiddleware, createFileExistsMiddleware('package.json'),
    createDockerMiddleware(opts.jobStore, opts.pendingContainer, {
      dockerCpDestinationPath: '/my-app',
      dockerCreateOpts: {
        Cmd: ['npm', 'install', '--unsafe-perm'],
        WorkingDir: '/my-app',
        HostConfig: {}
      }
    }))
  app.post('/monitor/project/yarn', handleBodyMiddleware, createFileExistsMiddleware('package.json'),
    createDockerMiddleware(opts.jobStore, opts.pendingContainer, {
      dockerCpDestinationPath: '/my-app',
      dockerCreateOpts: {
        Cmd: ['yarn', 'install'],
        WorkingDir: '/my-app',
        HostConfig: {}
      }
    }))
  app.post('/monitor/project/bundler', handleBodyMiddleware, createFileExistsMiddleware('Gemfile'),
    createBundlerMiddleware(),
    createDockerMiddleware(opts.jobStore, opts.pendingContainer, {
      dockerCpDestinationPath: '/my-app',
      dockerCreateOpts: {
        Cmd: ['bundle', 'install', '--no-binstubs', '--path', '/tmp'],
        WorkingDir: '/my-app',
        HostConfig: {}
      }
    }))
}
