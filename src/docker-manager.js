'use strict'

const Docker = require('dockerode')
const debug = require('debug')('pkgs:docker-manager')
const path = require('path')
const { spawnHelper } = require('./util.js')

// Falco
//   - make sure falco process is running
//   - receive events from falco

// Docker
//   - launch docker container, mount package tarballs as a volume
//   - automate call to npm install either via docker exec or inside of the container

const IMAGE_NAME = 'maldep'
const CONTAINER_NAME = 'some-container'
const CONTAIMER_PKG_PATH = '/tmp/packages'

class DockerManager {
  constructor (packagePath, opts) {
    this.docker = new Docker(opts && opts.dockerodeConfig)
    this.packagePath = packagePath
  }

  async _build () {
  // ToDo: Build image manually for now, maybe automate in future
  }

  async _create (target, createOpts = {}) {
    // Valid container names are [a-zA-Z0-9][a-zA-Z0-9_.-]*
    // Filter out other characters.
    // Ex. `some-container-hash!-0.0.0.tgz` -> 'some-container-hash-0.0.0.tgz'
    const name = (CONTAINER_NAME + '-' + target).replace(/[^a-zA-Z0-9_.-]*/g, '')
    debug(`creating container ${name} of image ${IMAGE_NAME}`)

    const defaultCreateOpts = {
      Cmd: ['npm', 'install', path.join(CONTAIMER_PKG_PATH, target)],
      Env: ['NO_UPDATE_NOTIFIER=1'], // supress the update notifier when starting npm
      Image: IMAGE_NAME,
      name: name,
      Tty: true,
      HostConfig: {
        AutoRemove: true, // reuse container. Hopefully fewer dependencies will need to be downloaded
        Mounts: [{
          Target: CONTAIMER_PKG_PATH,
          Source: this.packagePath,
          Type: 'bind',
          ReadOnly: true
        }]
      }
    }
    this.container = await this.docker.createContainer(Object.assign({}, defaultCreateOpts, createOpts))

    debug(`container ${this.container.id} created`)

    return this.container.id
  }

  // copies file to path in this.container
  async _cpIntoContainer (srcPath, dstPath) {
    // dockerode unfortunately doesn't support docker cp
    // use docker cli tool instead
    debug(`copy ${srcPath} into container ${this.container.id}:${dstPath}`)
    try {
      await spawnHelper('docker', ['cp', srcPath, this.container.id + ':' + dstPath])
    } catch (err) {
      debug('failed to copy into container: ', err)
      throw err
    }
  }

  async _start () {
    debug(`starting container ${this.container.id}`)
    await this.container.start()
    const stream = await this.container.attach({
      stream: true,
      stdout: true,
      stderr: true
    })

    await this._follow(stream)
  }

  async _stop () {
    if (!this.container) return // nothing to stop
    debug(`stoping container ${this.container.id}`)
    try {
      await this.container.stop(CONTAINER_NAME)
    } catch (err) {
      if (err.reason === 'container already stopped') return
      throw err
    }
  }

  async _remove () {
    if (!this.container) return // nothing to remove
    debug(`removing container ${this.container.id}`)
    try {
      await this.container.remove()
    } catch (err) {
      if (err.json && err.json.message &&
        err.json.message.indexOf('is already in progress') > -1) return
      throw err
    }
  }

  async _exec (tarball) {
    debug(`exec ${tarball} in container ${this.container.id}`)
    const exec = await this.container.exec({
      Cmd: ['npm', 'install', path.join(CONTAIMER_PKG_PATH, tarball)],
      AttachStdout: true,
      AttachStderr: true,
      Tty: true
    })
    await exec.start()
  }

  async _follow (stream) {
    stream.on('data', (chunk) => {
      debug(chunk.toString('utf8'))
    })

    await new Promise((resolve, reject) => {
      debug(`stdout of ${this.container.id} closed`)
      stream.on('close', resolve)
    })
  }

  async cleanup () {
    try {
      await this._stop()
      await this._remove()
    } catch (err) {
      if (err.reason && err.reason === 'no such container') return // noting to cle
      throw err
    }
  }
}

module.exports = DockerManager
