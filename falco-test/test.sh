#!/bin/sh 

# check that falco is running
pgrep falco
if [ $? -ne 0 ]; then
    echo "Falco is not running. Please start it manually before running the tests."
    exit
fi

file /var/log/syslog | grep -q 'no read permission'
if [ $? -eq 0 ]; then
    echo "usage: sudo ./test.sh"
    exit
fi

set -x

# make Falco use our custom rules
sudo cp ../falco/falco_rules.local.yaml /etc/falco/ && sudo service falco restart

# build and run the test image. This image simulates malicious 
# behavior that should get logged by Falco.
docker build -t maldep .

docker run --name maldep -p8080:8080 -it --rm maldep ./read-ssh-keys.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Error ssh-related file/directory read by non-ssh program"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./read-ssh-keys.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./read-env-vars.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Warning denylisted binary was executed"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./read-env-vars.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./read-npmrc.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Warning NPM config opened for reading"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./read-npmrc.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./read-user.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Warning denylisted binary was executed"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./read-user.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./read-sensitive-etc-files.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Warning Sensitive file opened for reading by non-trusted program"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./read-sensitive-etc-files.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./list-processes.sh
grep falco /var/log/syslog | tail -n1 | grep "Warning denylisted binary was executed" | grep -q "program=ps"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./list-processes.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./outbound-connection.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Notice Disallowed outbound connection destination"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./out.sh" && exit 1
fi

docker run --name maldep -p8080:8080 -it --rm maldep ./inbound-connection.sh
grep falco /var/log/syslog | tail -n1 | grep -q "Process started listening on a port"
if [ $? -ne 0 ]; then
    echo "TEST FAILED: ./inbound-connection.sh" && exit 1
fi

echo "TEST SUCCEEDED"