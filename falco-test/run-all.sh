#!/bin/bash -vx

# bash run-code.sh
bash read-ssh-keys.sh
bash read-env-vars.sh
bash read-npmrc.sh
bash read-user.sh
bash read-sensitive-etc-files.sh
bash list-processes.sh
bash outbound-connection.sh
bash inbound-connection.sh

exit 0
